package dao

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/scanner"
	"strings"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

const (
	DriverMysql    = "mysql"
	DriverPostgres = "postgres"
	DriverSqlite3  = "sqlite3"
	DriverRamsql   = "ramsql"
)

//go:generate mockgen -source=./dao.go -destination=./mock/dao_mock.go -package=mock
type DAOFace interface {
	Begin() (*sqlx.Tx, error)
	Ping(ctx context.Context) error
	Create(ctx context.Context, entity tabler.Tabler, opts ...interface{}) error
	Upsert(ctx context.Context, entities []tabler.Tabler, opts ...interface{}) error
	GetCount(ctx context.Context, entity tabler.Tabler, condition params.Condition, opts ...interface{}) (uint64, error)
	List(ctx context.Context, dest interface{}, table tabler.Tabler, condition params.Condition, opts ...interface{}) error
	Update(ctx context.Context, entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) error
	Exec(ctx context.Context, query string, tx *sqlx.Tx, args ...interface{}) (sql.Result, error)
	LockTable(ctx context.Context, tableName string, mode string, tx *sqlx.Tx) error
}

type DAO struct {
	db         *sqlx.DB
	scanner    scanner.Scanner
	sqlBuilder sq.StatementBuilderType
}

func NewDAO(db *sqlx.DB, dbConf params.DB, scanner scanner.Scanner) *DAO {
	var builder sq.StatementBuilderType
	if dbConf.Driver != "mysql" {
		builder = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	}

	return &DAO{db: db, scanner: scanner, sqlBuilder: builder}
}

func (s *DAO) Begin() (*sqlx.Tx, error) {
	return s.db.Beginx()
}

func (s *DAO) Exec(ctx context.Context, query string, tx *sqlx.Tx, args ...interface{}) (sql.Result, error) {
	if tx != nil {
		return tx.ExecContext(ctx, query, args...)
	}
	return s.db.ExecContext(ctx, query, args...)
}

// LockTable locks the table for write
func (s *DAO) LockTable(ctx context.Context, tableName string, mode string, tx *sqlx.Tx) error {
	query := fmt.Sprintf("LOCK TABLE %s IN %s MODE", tableName, mode)
	_, err := s.Exec(ctx, query, tx)

	return err
}

func (s *DAO) Ping(ctx context.Context) error {
	return s.db.PingContext(ctx)
}

func (s *DAO) Create(ctx context.Context, table tabler.Tabler, opts ...interface{}) error {
	createFields, createFieldsPointers := s.getFields(table, scanner.Create)

	queryRaw := s.sqlBuilder.Insert(table.TableName()).Columns(createFields...).Values(createFieldsPointers...)

	query, args, err := queryRaw.ToSql()
	if err != nil {
		return err
	}

	if tx := getTransaction(opts...); tx != nil {
		_, err = tx.ExecContext(ctx, query, args...)
	} else {
		_, err = s.db.ExecContext(ctx, query, args...)
	}

	return err
}

func (s *DAO) getFields(entity tabler.Tabler, operation string) ([]string, []interface{}) {
	fields := s.scanner.OperationFields(entity, operation)
	var fieldsPointers []interface{}
	var fieldsName []string
	for i := range fields {
		fieldsPointers = append(fieldsPointers, fields[i].Pointer)
		fieldsName = append(fieldsName, fields[i].Name)
	}

	return fieldsName, fieldsPointers
}

func (s *DAO) Upsert(ctx context.Context, entities []tabler.Tabler, opts ...interface{}) error {
	if len(entities) < 1 {
		return fmt.Errorf("SQL adapter: zero entities passed")
	}
	createFields, _ := s.getFields(entities[0], scanner.Create)
	queryRaw := s.sqlBuilder.Insert(entities[0].TableName()).Columns(createFields...)

	for i := range entities {
		_, createFieldsPointers := s.getFields(entities[i], "create")
		queryRaw = queryRaw.Values(createFieldsPointers...)
	}

	query, args, err := queryRaw.ToSql()
	if err != nil {
		return err
	}

	conflictFields, _ := s.getFields(entities[0], scanner.Conflict)
	if len(conflictFields) > 0 {
		query = query + " ON CONFLICT (%s)"
		query = fmt.Sprintf(query, strings.Join(conflictFields, ","))
		query = query + " DO UPDATE SET"
	}
	upsertFields, _ := s.getFields(entities[0], scanner.Upsert)
	for _, field := range upsertFields {
		query += fmt.Sprintf(" %s = excluded.%s,", field, field)
	}
	if len(upsertFields) > 0 {
		query = query[0 : len(query)-1]
	}

	if tx := getTransaction(opts...); tx != nil {
		_, err = tx.ExecContext(ctx, query, args...)
	} else {
		_, err = s.db.ExecContext(ctx, query, args...)
	}

	return err
}

func (s *DAO) buildSelect(tableName string, condition params.Condition, fields ...string) (string, []interface{}, error) {
	if condition.ForUpdate {
		temp := []string{"FOR UPDATE"}
		temp = append(temp, fields...)
		fields = temp
	}
	queryRaw := s.sqlBuilder.Select(fields...).From(tableName)

	if condition.Equal != nil {
		for field, args := range condition.Equal {
			queryRaw = queryRaw.Where(sq.Eq{field: args})
		}
	}

	if condition.NotEqual != nil {
		for field, args := range condition.NotEqual {
			queryRaw = queryRaw.Where(sq.NotEq{field: args})
		}
	}

	if condition.Order != nil {
		for _, order := range condition.Order {
			direction := "DESC"
			if order.Asc {
				direction = "ASC"
			}
			queryRaw = queryRaw.OrderBy(fmt.Sprintf("%s %s", order.Field, direction))
		}
	}

	if condition.LimitOffset != nil {
		if condition.LimitOffset.Limit > 0 {
			queryRaw.Limit(uint64(condition.LimitOffset.Limit))
		}
		if condition.LimitOffset.Offset > 0 {
			queryRaw.Offset(uint64(condition.LimitOffset.Offset))
		}
	}

	return queryRaw.ToSql()
}

func (s *DAO) GetCount(ctx context.Context, entity tabler.Tabler, condition params.Condition, opts ...interface{}) (uint64, error) {
	query, args, err := s.buildSelect(entity.TableName(), condition, "COUNT(*)")
	if err != nil {
		return 0, err
	}

	var rows *sqlx.Rows
	if tx := getTransaction(opts...); tx != nil {
		rows, err = tx.QueryxContext(ctx, query, args...)
	} else {
		rows, err = s.db.QueryxContext(ctx, query, args...)
	}

	if err != nil {
		return 0, err
	}

	var count uint64
	// iterate over each row
	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			return 0, err
		}
	}
	// check the error from rows
	err = rows.Err()

	return count, err
}

func (s *DAO) List(ctx context.Context, dest interface{}, table tabler.Tabler, condition params.Condition, opts ...interface{}) error {
	fields, _ := s.getFields(table, scanner.AllFields)
	query, args, err := s.buildSelect(table.TableName(), condition, fields...)
	if err != nil {
		return err
	}

	if tx := getTransaction(opts...); tx != nil {
		err = tx.SelectContext(ctx, dest, query, args...)
	} else {
		err = s.db.SelectContext(ctx, dest, query, args...)
	}

	return err
}

func (s *DAO) Update(ctx context.Context, entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) error {
	ent := entity
	updateFields, updateFieldsPointers := s.getFields(entity, operation)

	updateRaw := s.sqlBuilder.Update(ent.TableName())

	if condition.Equal != nil {
		for field, args := range condition.Equal {
			updateRaw = updateRaw.Where(sq.Eq{field: args})
		}
	}

	if condition.NotEqual != nil {
		for field, args := range condition.NotEqual {
			updateRaw = updateRaw.Where(sq.NotEq{field: args})
		}
	}

	for i := range updateFields {
		updateRaw = updateRaw.Set(updateFields[i], updateFieldsPointers[i])
	}

	query, args, err := updateRaw.ToSql()
	if err != nil {
		return err
	}

	var res sql.Result
	if tx := getTransaction(opts...); tx != nil {
		res, err = tx.ExecContext(ctx, query, args...)
	} else {
		res, err = s.db.ExecContext(ctx, query, args...)
	}
	if err != nil {
		return err
	}
	_, err = res.RowsAffected()

	return err
}

func getTransaction(opts ...interface{}) *sqlx.Tx {
	if len(opts) > 0 {
		if tx, ok := opts[0].(*sqlx.Tx); ok {
			return tx
		}
	}
	for _, opt := range opts {
		switch tx := opt.(type) {
		case *sqlx.Tx:
			return tx
		}
	}
	return nil
}
